/* EdgeList To AdjacencyMatrix */

#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    
    vector<vector<int>> v(n, vector<int>(n, 0));
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        v[a-1][b-1] = 1;
        v[b-1][a-1] = 1;
    }
    
    for (const auto& r : v) {
        for (const auto& a : r) {
            cout << a;
        }
        cout << endl;
    }
    return 0;
}
