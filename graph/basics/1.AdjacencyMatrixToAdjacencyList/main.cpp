/* Adjacency matrix to adjacency list */

#include <numeric>
#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n;
    cin >> n;
    char dummy[256];
    cin.getline(dummy, 256);
    vector<vector<int>> m(n, vector<int>(n, 0));
    
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            char c;
            cin.read(&c, sizeof(char));
            m[i][j] = int(c - '0');
        }
        cin.getline(dummy, 256);
    }
    
    for (int i = 0; i < n; ++i) {
        cout << accumulate(m[i].begin(), m[i].end(), 0) << " ";
        for (int j = 0; j < n; ++j) {
            if (m[i][j] == 1) {
                cout << j+1 << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
