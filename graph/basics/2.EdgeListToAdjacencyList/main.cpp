/* EdgeList To AdjacencyList */

#include <iostream>
#include <set>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    
    vector<set<int>> v(n);
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        v[a-1].insert(b);
        v[b-1].insert(a);
    }
    
    for (int i = 0; i < n; ++i) {
        cout << v[i].size() << " ";
        for (const auto& a : v[i]) {
            cout << a << " ";
        }
        cout << endl;
    }
    return 0;
}
