/*
  Компоненты связности
  Дан неориентированный граф, заданный своим списком ребер.
  Требуется определить компоненты связности в нем.

  В первой строке записано два целых числа N, M (1≤N≤20000, 1≤M≤200000)
  - количество вершин и количество ребер.

  В следующих M строках описаны ребра графа. Каждое ребро задается двумя
  числами u, v (1≤u,v≤N) - номера концов ребра.
  
  На первой строке выходного файла выведите число L - количество компонент
  связности заданного графа. На следующей строке выведите N чисел из диапазона
  от 1 до L - номера компонент связности, которым принадлежат соответствующие
  вершины. Компоненты связности следует занумеровать от 1 до L в порядке
  возрастания минимального номера вершины в компоненте.
 */

#include <iostream>
#include <vector>

using namespace std;

void dfs(vector<vector<int>>& adj,
	 vector<int>& explored,
	 int visited,
	 int label) {
    explored[visited] = label;
    for (auto adjacent : adj[visited]) {
        if (explored[adjacent]) {
            continue;
        }
        dfs(adj, explored, adjacent, label);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m; 
    cin >> n >> m;
    vector<vector<int>> adj(n);
    vector<int> explored(n, 0);
        
    for (int i = 0; i < m; ++i) {
        int u,v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    
    int number = 0;
    for (int i = 0; i < n; ++i) {
        if (explored[i]) {
            continue;
        }
        dfs(adj, explored, i, ++number);
    }
            
    cout << number << endl;
    for (auto v : explored) {
        cout << v << " ";
    }
    
    return 0;
}
