/*
  Есть ли цикл?
  В данной задаче вам нужно проверить, есть ли цикл в неориентированном графе.

  В первой строке записано два целых числа n, m (1≤n≤500;0 ≤m≤2n⋅(n−1)) - количество
  вершин и количество ребер.
  В следующих m строках описаны ребра графа. Каждое ребро задается двумя числами u, v
  1≤u,v≤n u!=v - номера концов ребра. Гарантируется, что каждое ребро описано в графе
  единожды.

  В единственной строке выведите YES (без кавычек), если цикл есть и NO (без кавычек) иначе.
 */

#include <iostream>
#include <vector>

using namespace std;

bool dfs(vector<vector<int>>& adj, 
         vector<bool>& explored,
         int parent,
         int visited) {
    explored[visited] = true;
    for (auto v : adj[visited]) {
        if (explored[v]) {
            if (v != parent) return true;
        }
        else {
            if (dfs(adj, explored, visited, v)) return true;
        }
    }
    return false;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    
    vector<vector<int>> adj(n);
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    
    vector<bool> explored(n, false);
        
    bool hasCycle = false;
    for (int i = 0; i < n; ++i) {
        if (!explored[i]) {
            hasCycle = dfs(adj, explored, -1, i);
            if (hasCycle) break;
        }
    }
    cout << (hasCycle ? "YES" : "NO") << endl;
    return 0;
}
