/*
  Пошаговый обход графа
  Пошаговым обходом графа из вершины v назовём последовательность вершин
  u_1, u_2 ... u_r 
  такую, что:

  1) u_1 = u_r = v 
  2) Каждая вершина графа, достижимая из vv, встречается в ней хотя бы один раз, и
  3) Между любыми двумя соседними вершинами последовательности в графе существует ребро.

  Задан связный неориентированный граф и его вершина v. Выведите любой пошаговый
  обход этого графа.
  В первой строке входного файла записаны два целых числа n, m (1≤n≤100;0 ≤m≤10000) -
  количество вершин и ребер в графе.

  Следующие m строк содержат по два числа через пробел u_i, v_i;
  каждая такая строчка означает, что в графе существует ребро между вершинами u_i и v_i.

  Последняя строчка содержит единственное целое число v (1≤v≤n) - вершина,
  из которой следует начинать обход.

  Выходные данные

  В первой строке выведите единственное целое число r - количество вершин в
  найденном пошаговом обходе(1≤t≤10000; гарантируется, что обход, удовлетворяющий
  этим ограничениям, существует).

  Во второй строке выведите сами числа u_1, u_2, ... u_r.
 */

#include <iostream>
#include <vector>

using namespace std;

void dfs(vector<vector<int>>& adj, 
         vector<bool>& explored,
         vector<int>& res,
         int u) {
    explored[u] = true;
    for (const auto& v : adj[u]) {
        if (explored[v]) {
            continue;
        }
        res.push_back(v);
        dfs(adj, explored, res, v);
        res.push_back(u);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    vector<vector<int>> adj(n);
    int start;
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    cin >> start;
    vector<bool> explored(n, false);
       
    vector<int> res;
    res.push_back(start-1);
    dfs(adj, explored, res, start-1);
    
    cout << res.size() << endl;
    for (auto v : res) {
        cout << v+1 << " ";
    }
    return 0;
}
