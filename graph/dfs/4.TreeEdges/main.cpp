/*
  Дерево обхода

  Дан связный неориентированный граф, заданный своим списком ребер.
  Требуется выделить дерево обхода графа в глубину.

  В первой строке записано два целых числа n, m (1≤n≤500;0 ≤m≤ 2 n⋅(n−1))
  - количество вершин и количество ребер.
  В следующих m строках описаны ребра графа. Каждое ребро задается двумя
  числами u, v 1≤u,v≤n;u != v) - номера концов ребра. Гарантируется, что
  каждое ребро описано в графе единожды.

  В первой строке выведите единственное число - количество древесных ребер q.

  В следующей строке выведите q чисел - номера древесных ребер. Все номера
  должны быть различны и лежать в промежутке 1…m. Ребра нумеруются с единицы
  в том порядке, в котором они даны во входных данных. Если деревьев обхода
  графа в глубину несколько, выведите любой.
 */

#include <iostream>
#include <vector>

using namespace std;

struct Edge {
    int to;
    int id;
};

void dfs(vector<vector<Edge>>& adj, 
         vector<bool>& explored, 
         vector<int>& path, 
         int visited)
{
    explored[visited] = true;
    for (const auto& v : adj[visited]) {
        if (explored[v.to]) {
            continue;
        }
        path.push_back(v.id);
        dfs(adj, explored, path, v.to);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    
    vector<vector<Edge>> adj(n);
    vector<bool> explored(n, false);
    
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back({v-1, i+1});
        adj[v-1].push_back({u-1, i+1});
    }
    vector<int> path;
    dfs(adj, explored, path, 0);
    cout << path.size() << endl;
    for (auto v : path) {
        cout << v << " ";
    }
    cout << endl;
    return 0;
}
