/*
  Обратные ребра

  Дан связный неориентированный граф, заданный своим списком ребер.
  Требуется определить количество обратных ребер в этом графе.

  В первой строке записано два целых числа n, m (1≤n≤500;0 ≤m≤ 2 n⋅(n−1))
  - количество вершин и количество ребер.
  В следующих m строках описаны ребра графа. Каждое ребро задается двумя
  числами u, v (1≤u,v≤n) - номера концов ребра. Гарантируется, что каждое
  ребро описано в графе единожды.

  Выведите единственное целое число - количество обратных ребер в графе.
  Если возможных ответов несколько, выведите любой.

  ! Очевидное решение: m - (n-1)
 */

#include <iostream>
#include <vector>



using namespace std;

void dfs(vector<vector<int>>& adj, 
         vector<bool>& explored, 
         int visited,
         int parent,
         int& count) {
    explored[visited] = true;
    for (auto v : adj[visited]) {
        if (explored[v]) {
            if (v != parent) {
                ++count;
            }
            continue;
        }
        dfs(adj, explored, v, visited, count);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    
    vector<vector<int>> adj(n);
    vector<bool> explored(n , false);
    
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    int number = 0;
    dfs(adj, explored, 0, -1, number);
    
    cout << number/2 << endl;
    return 0;
}
