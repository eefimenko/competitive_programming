/*
  Владислав заметил, что в 20202020-м году целых две пары
  одинаковых символов - 22 и 00. На основании этого он возненавидел
  цифры 22 и 00, а также ситуацию, когда в году есть одинаковые цифры.
  Сейчас Владислав ждет следующего года, в котором нет цифр 22 и 00,
  а также все цифры попарно различны. Посчитайте, в каком году это
  случится первый раз?

  Входные данные:

  В первой строке входных данных записано единственное целое число n
  (0 \leq n \leq 10^90≤n≤10^9) - текущий год.

  Выходные данные:

  Выведите одно число - следующий год, в котором все цифры попарно
  различны, а также нет цифр 22 и 00. Если больше такого года никогда
  не будет, выведите <<-1>> (без кавычек).
*/

#include <iostream>
#include <unordered_set>

using namespace std;

bool isValid(long year) {
    unordered_set<int> s;
    
    while (year != 0) {
        int digit = year % 10;
        year /= 10;
        if (digit == 2 || digit == 0) {
            return false;
        }
        if (s.count(digit) > 0) {
            return false;
        }
        s.insert(digit);
    }
    return true;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    long year;
    cin >> year;
    year++;
    
    while(!isValid(year) && year < 100000000) {
        year++;
    }
    if (year >= 100000000) {
        cout << -1 << endl;
    }
    else {
        cout << year << endl;
    }
    return 0;
}
