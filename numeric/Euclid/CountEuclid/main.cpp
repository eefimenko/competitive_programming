/*
  Даны два целых положительных числа a>b, не превосходящие 10^9.
  Сколько раз в алгоритме Евклида при вычислении их НОД будет
  использована операция взятия остатка?
 */

#include <iostream>

using namespace std;

int gcd(int a, int b, int count) {
    if (b == 0) {
        return count;
    }
    else {
        return gcd(b, a % b, count + 1);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int a, b;
    cin >> a >> b;
    
    cout << gcd(a, b, 0) << endl;
    return 0;
}
