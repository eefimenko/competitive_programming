/*
Во входном файле заданы два целых положительных числа a и b,
не превосходящие 10^{18}. Если их наименьшее общее кратное
(то есть наименьшее целое положительное число, делящееся
и на a, и на b) не превосходит 10^{18}, выведите его значение.
В противном случае выведите -1.
 */

#include <iostream>

using namespace std;

unsigned long long gcd(unsigned long long a, unsigned long long b) {
    if (a < b) return gcd(b, a);
    if (b == 0) return a;
    return gcd(b, a % b);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    unsigned long long a, b;
    cin >> a >> b;
    unsigned long long d = 1000000000000000000;
    
    unsigned long long c  = gcd(a, b);
    if (a/c < d/b) {
        cout << a/c*b << endl;
    }
    else {
        cout << -1 << endl;
    }
    return 0;
}
