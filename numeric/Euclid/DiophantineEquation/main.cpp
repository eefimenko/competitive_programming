/*
  По заданным целым a, b и c (0<|a|,|b|,|c| < 10^6) вычислите,
  имеет ли уравнение ax+by=c хотя бы одно целочисленное решение.
  Если у уравнения есть решение, выведите 1, если нет --- выведите 0.
 */
#include <iostream>

using namespace std;

int gcd(int a, int b) {
    if (a < b) return gcd(b, a);
    if (b == 0) return a;
    return gcd(b, a % b);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int a, b, c;
    cin >> a >> b >> c;
    
    int r = gcd(abs(a), abs(b));
    if (abs(c) % r == 0) {
        cout << 1 << endl;
    }
    else {
        cout << 0 << endl;
    }
    return 0;
}
