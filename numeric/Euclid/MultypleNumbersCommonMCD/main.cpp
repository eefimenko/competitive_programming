/*
  Найти наибольший общий делитель N целых положительных чисел.
  В первой строке входных данных находится одно целое положительное
  число N, не меньшее 2 и не превосходящее 100. Вторая строка
  содержит N целых положительных чисел, не превосходящих 10^6,
  наибольший общий делитель которых надо найти.
 */

#include <iostream>

using namespace std;

int gcd(int a, int b) {
    if (a < b) return gcd(b, a);
    if (b == 0) return a;
    return gcd(b, a % b);
}
    

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, a, b;
    cin >> n;
    cin >> a;
    
    for (int i = 1; i < n; ++i) {
        cin >> b;
        a = gcd(a, b);
    }
    cout << a << endl;
    return 0;
}
