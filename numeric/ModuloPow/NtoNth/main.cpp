/*
  Вам даны два целых числа N и M (1≤N≤10^9 ,2≤M≤10^9).
  Вычислите остаток от деления N^N на M.
 */

#include <iostream>

using namespace std;

long long pow(long long a, long long n, long long mod) {
    if (n == 0) return 1;
    if (n % 2 == 1) return a * pow(a, n -1, mod) % mod;
    long long tmp = pow(a, n/2, mod) % mod;
    return (tmp * tmp) % mod;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> n >> m;
    cout << pow(n, n, m) << endl;
    return 0;
}
