/*
  Во входном файле заданы два неотрицательных целых числа a и b,
  не превосходящие 10^5. Вычислите остаток от деления разности a^2-b^2a 
  на 10^6+7
 */

#include <iostream>

using namespace std;

long long norm(long long num, long long mod) {
    return ((num % mod) + mod) % mod;
}

long long mult(long long a, long long b, long long mod) {
    return norm(norm(a, mod) * norm(b, mod), mod);
}

long long add(long long a, long long b, long long mod) {
    return norm(norm(a, mod) + norm(b, mod), mod);
}

long long sub(long long a, long long b, long long mod) {
    return norm(norm(a, mod) - norm(b, mod), mod);
}

int main() {
    int a,  b;
    cin >> a >> b;
    int mod = 1000007;
    cout << sub(mult(a, a, mod), mult(b, b, mod), mod) << endl;
    return 0;
}
