/* Последовательность Фибоначчи задаётся следующим образом.
   F(0)=F(1)=1, для всех N>1 F(N)=F(N-1)+F(N-2). Вам задано
   целое число N, 1≤N≤10^4.
   Вычислите остаток от деления F(N) на 10^6+3
*/

#include <iostream>

using namespace std;

pair<long long, long long> fib(pair<long long, long long>& p) {
    pair<long long, long long> tmp;
    tmp.first = p.second;
    tmp.second = p.first + p.second;
    return tmp;
}

pair<long long, long long> fib_modulo(pair<long long, long long>& p, int mod) {
    pair<long long, long long> tmp;
    tmp.first = p.second;
    tmp.second = (p.first + p.second) % mod;
    return tmp;
}

int main() {
    int n;
    cin >> n;
    pair<long long, long long> p;
    p.first = 1;
    p.second = 1;
    
    for (int i = 1; i < n; ++i) {
        p = fib_modulo(p, 1000003);
    }
    cout << p.second  << endl;
    return 0;
}
