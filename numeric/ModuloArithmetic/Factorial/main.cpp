/*
  Во входном файле задано целое число N, не превосходящее 10^9.
  Найдите остаток от деления числа N! (произведения всех чисел
  от 1 до N) на 10^6+3
 */
#include <iostream>

using namespace std;

long long norm(long long num, long long mod) {
    return ((num % mod) + mod) % mod;
}

long long mult(long long a, long long b, long long mod) {
    return norm(norm(a, mod) * norm(b, mod), mod);
}

long long add(long long a, long long b, long long mod) {
    return norm(norm(a, mod) + norm(b, mod), mod);
}

long long sub(long long a, long long b, long long mod) {
    return norm(norm(a, mod) - norm(b, mod), mod);
}

int main() {
    long long n;
    cin >> n;
    long long mod = 1000003;
    long long ans = 1;
    if (n >= mod) {
        ans = 0;
    }
    else {
        for (int i = 1; i <= n; ++i) {
            ans = mult(ans, i, mod);
        }
    }
    cout << ans << endl;
    return 0;
}
