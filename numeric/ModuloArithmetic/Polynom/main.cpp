/*
  На входе задан многочлен f(x)f(x) вида a_nx^n+a_{n-1}x^{n-1}+\ldots+a_1x+a_0:
  в первой строке задано одно целое число - степень многочлена n  (1≤n≤1000),
  а также некоторое целое число M (2≤M≤2025), во второй задаются n+1 целых чисел -
  коэффициенты многочлена, начиная с a_n и заканчивая a_0 (−10^6≤a_i≤10^6).
  Найдите такое минимальное целое неотрицательное xx, что f(x) делится на MM,
  или выведите -1 в случае, если таких x нет.
 */

#include <iostream>

using namespace std;
const int N = 2000;
int array[N];

long long norm(long long num, long long mod) {
    return ((num % mod) + mod) % mod;
}

long long mult(long long a, long long b, long long mod) {
    return norm(norm(a, mod) * norm(b, mod), mod);
}

long long add(long long a, long long b, long long mod) {
    return norm(norm(a, mod) + norm(b, mod), mod);
}

long long sub(long long a, long long b, long long mod) {
    return norm(norm(a, mod) - norm(b, mod), mod);
}

long long calculate_remainder(long long x, int n, long long mod) {
    long long res = 0;
    long long m = norm(1, mod);
    for (int i = 0; i <= n; ++i) {
        res = add(res, mult(array[i], m, mod), mod);
        m = mult(m, x, mod);
    }
    return res;
}

int main() {
    int n, m;
    cin >> n >> m;
    
    for (int i = n; i >=0; --i) {
        cin >> array[i];
    }
    
    bool found = false;
    int i;
    for (i=0; i <= m; ++i) {
        long long r = calculate_remainder(i, n, m);
        if (r == 0) {
            found = true;
            break;
        }
    }
    cout << (found ? i : -1) << endl;
    return 0;
}
