/*
  Задано целое число N (4≤N≤10^6).
  Найти сумму наименьших простых делителей всех составных чисел,
  больших 2 и не превосходящих N.
 */
#include <iostream>

const int N = 1000010;
int array[N];

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n;
    cin >> n;
    long long sum = 0;
    for (int i = 2; i*i <= n; ++i) {
        if (array[i] == 0) {
            for (int j = i*i; j <=n; j = j+i) {
                if (array[j] == 0)
                {
                    array[j] = i;
                    sum += i;
                }
            }
        }
    }
    cout << sum << endl;
    return 0;
}
