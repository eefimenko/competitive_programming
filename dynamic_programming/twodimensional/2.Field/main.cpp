/*
  Поле

  Отряду нужно пересечь прямоугольное поле размера m×n квадратов,
  двигаясь из левого верхнего угла в правый нижний и перемещаясь
  между соседними квадратами только в двух направлениях - вправо
  и вниз. Поле не очень ровное, но у отряда есть карта, на которой
  отмечена высота каждого квадрата. Опасность перехода с квадрата
  высоты h_1 на соседний квадрат высоты h_2​	
  оценивается числом |h_2 - h_1|; опасность всех переходов в пути
  суммируется. Выясните, какова минимальная опасность пути из
  квадрата (1,1) в квадрат (m,n).
  В первой строке входного файла заданы два числа m и n через
  пробел (1≤m,n≤100). В следующих n строках записано по m чисел в
  каждой; i-ое число j-ой из этих строк соответствует высоте
  квадрата (i,j). Все высоты - целые числа в диапазоне от 1 до 100,
  включительно.
  Выведите в выходной файл одно число - минимальную опасность пути
  из квадрата (1,1) в квадрат (m,n).
 */

#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n, m;
    cin >> m >> n;
    
    vector<vector<int>> matrix(n+1, vector<int>(m+1, 0));
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    
    for (int i=1; i <=n; ++i) {
        for (int j=1; j <=m; ++j) {
            cin >> matrix[i][j];
        }    
    }
    dp[1][1] = 0;
    for (int i = 2; i <=n; ++i) {
        dp[i][1] = dp[i-1][1] + abs(matrix[i][1] - matrix[i-1][1]);
    }
    
    for (int j = 2; j <= m; ++j) {
        dp[1][j] = dp[1][j-1] + abs(matrix[1][j] - matrix[1][j-1]);
    }
    
    for (int i = 2; i <=n; ++i) {
        for (int j = 2; j <= m; ++j) {
            dp[i][j] = min(dp[i][j-1] + abs(matrix[i][j] - matrix[i][j-1]),
                           dp[i-1][j] + abs(matrix[i][j] - matrix[i-1][j]));
        }
    }
    cout << dp[n][m] << endl;
    return 0;                
}


