/*
  Путь Черепахи

  В верхнем-левом углу таблицы сидит черепаха. Она умеет ходить
  только вниз, вправо и вниз-вправо. Каждая клетка доски либо
  свободна, либо занята. Черепаха хочет попасть в нижнюю-правую
  клетку. Сколькими способами она может это сделать?

  В первой строке входных данных записано два числа n, m (1≤n,m≤1000)
  - количество строк и столбцов в таблице.
  В следующих n строках записано по m чисел - данная таблица. 0
  обозначает свободную клетку, а 1 - занятую. Гарантируется, что
  верхняя левая и нижняя правая клетки свободны.
  Выведите единственное число - количество способов добраться до
  нижней правой клетки. Поскольку это число может быть очень большим,
  выведите его по модулю 10^9 + 9.
 */

#include <iostream>
#include <vector>

using namespace std;

int norm(int a, int mod) {
    return ((a % mod) + mod) % mod;
}

int add(int a, int b, int mod) {
    return norm(norm(a, mod) + norm(b, mod), mod);
}

int main() {
    int mod = 1000000000 + 9; 
    int n, m;
    cin >> n >> m;
    vector<vector<int>> grid(n+1, vector<int>(m+1, 0));
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            cin >> grid[i][j];
        }
    }
    dp[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            if (grid[i][j] != 1) {
                dp[i][j] = add(add(dp[i][j-1], dp[i-1][j], mod), dp[i-1][j-1], mod);
            }
        }
    }
    cout << dp[n][m] << endl;
    return 0;
}
