#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

int norm(int a, int mod) {
    return a % mod;
}

void print(vector<vector<int>>& dp) {
    int n = dp.size();
    int m = dp[0].size();
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << dp[i][j] << " ";
        }
        cout << "\n";
    }
}

int main() {
    int m, n;
    cin >> n >> m;
    int mod = 1000000000 + 123;
    
    vector<vector<int>> dp(n+3, vector<int>(m+3, 0));
    dp[2][2] = 1;
 
    for (int k = 1; k <= (n+m); ++k) {
        for (int q = 0; q <= k; ++q) {
            int i = q + 2;
            int j = k - q + 2;
            
            if (i > n + 1) continue;
            if (j > m + 1) continue;
	    int tmp1 = norm(dp[i-2][j+1] + dp[i-2][j-1], mod);
	    int tmp2 = norm(dp[i-1][j-2] + dp[i+1][j-2], mod);
	    dp[i][j] = norm(tmp1 + tmp2, mod);
	}
    }

    cout << dp[n+1][m+1] << endl;
    return 0;
}
