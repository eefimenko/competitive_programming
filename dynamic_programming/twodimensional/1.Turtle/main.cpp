/*
  Черепашка

  В верхнем-левом углу таблицы сидит черепаха. Она умеет ходить только вниз,
  вправо и вниз-вправо. В каждой клетке записано число, и черепашка прибавляет
  его к своей сумме, когда оказывается на клетке. Черепаха хочет попасть в
  нижнюю-правую клетку, получив в итоге минимально возможную сумму. Помогите
  ей - посчитайте, какую сумму она в итоге наберет?

  В первой строке входных данных записано два числа n, m (1≤n,m≤1000) -
  количество строк и столбцов в таблице.
  В следующих n строках записано по m чисел - данная таблица. Каждое
  число в таблице по модулю не превышает 1000. 
  Выведите единственное число - минимальную сумму, которую может
  набрать черепашка.
 */

#include <iostream>
#include <vector> 

using namespace std;

void print(vector<vector<int>>& dp) {
    for (int i = 0; i < dp.size(); ++i) {
        for (int j = 0; j < dp[0].size(); ++j) {
            cout << dp[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n,m;
    cin >> n >> m;
    vector<vector<int>> matrix(n+1, vector<int>(m+1, 0));
    vector<vector<int>> dp(n+1, vector<int>(m+1, 0));
    
    for (int i = 1; i <=n; ++i) {
        for (int j = 1; j <= m; ++j) {
            cin >> matrix[i][j];
        }
    }
    
    for (int i = 1; i <=n; ++i) {
        dp[i][1] = dp[i-1][1] + matrix[i][1];
    }
    
    for (int j = 1; j <=m; ++j) {
        dp[1][j] = dp[1][j-1] + matrix[1][j];
    }
    
    for (int i = 2; i <=n; ++i) {
        for (int j = 2; j <=m; ++j) {
            dp[i][j] = min(min(dp[i][j-1], dp[i-1][j]), dp[i-1][j-1]) + matrix[i][j];
        }  
    }
    cout << dp[n][m] << endl;
    return 0;
}

