/*
  Строкой Фибоначчи называется строка, состоящая только из 0 и 1,
  в которой не встречается двух 1 подряд. Вам требуется найти
  количество таких строк длины n.
  Во входных данных записано одно число n (0≤n≤90) - длина строки.
  Выведите одно число - количество строк Фибоначчи длины n.
 */

#include <iostream>
#include <vector>

using namespace std;

vector<long long> dp;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n;
    cin >> n;
    
    dp.assign(n + 1, 0);
    dp[0] = 1;
    dp[1] = 2;
    for (int i = 2; i <=n; ++i) {
        dp[i] = dp[i-1] + dp[i-2];
    }
    cout << dp[n] << endl;
    return 0;
}
