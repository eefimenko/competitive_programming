/*
  Перед клетчатой полоской длины n сидит кузнечик. Каждая
  клетка является либо занятой, либо свободной. Кузнечик
  умеет прыгать на 1, 2 и 3 клетки вперед. Найдите количество
  различных путей, которыми он может добраться до последней
  клетки, не заходя в занятые.
  В первой строке записано единственное целое число n (1≤n≤5⋅10^5).
  Во второй строке записана строка длины nn, состоящая только из
  нулей и единиц. Ноль обозначает свободную клетку, а единица - занятую.
  Выведите единственное число - количество способов добраться до
  последней клетки. Поскольку это число может быть очень большим,
  выведите его по модулю 10^9 + 7.
 */

#include <iostream>
#include <vector>
#include <string>

using namespace std;
vector<long long> dp;

long long add(long long a, long long b, long long mod) {
    return (a % mod + b % mod) % mod;
}

long long f(int i, const string& a) {
    long long mod = 1000000000 + 7;
    if (i < 0) {
        return 0;
    }
    
    if (i == 0) {
        return 1;
    }
    
    if (dp[i] == -1) {
        dp[i] = (a[i-1] == '0') ? add(add(f(i - 1, a), f(i - 2, a), mod), f(i - 3, a), mod) : 0;
    }

    return dp[i];
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n;
    string a;
    cin >> n >> a;
    dp.assign(n + 1, -1);
    
    cout << f(n, a) << endl;
    return 0;
}
