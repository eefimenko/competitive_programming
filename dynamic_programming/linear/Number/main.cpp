/*
  Имеется калькулятор, который выполняет следующие операции:

  Умножить число X на 2.
  Умножить число X на 3.
  Прибавить к числу X единицу.
  Определите, какое наименьшее количество операций требуется,
  чтобы получить из числа 1 число N.
 */

#include <iostream>

using namespace std;

const int N = 1000010;
long long dp[N];

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int n;
    cin >> n;
    
    dp[0] = 0;
    dp[1] = 0;
    for (int i = 2; i <= n; ++i) {
        long long m = dp[i - 1] + 1;
        if (i%3 == 0) {
            m = min(m, dp[i/3] + 1);
        }
        if (i%2 == 0) {
            m = min(m, dp[i/2] + 1);
        }
        dp[i] = m;
    }
    cout << dp[n] << endl;
    return 0;
}
