/*
  Перед клетчатой полоской длины n сидит кузнечик. Каждая
  клетка является либо занятой, либо свободной. Кузнечик
  умеет прыгать на 1, 2, … k клеток вперед. Найдите количество
  различных путей, которыми он может добраться до последней
  клетки, не заходя в занятые.
  В первой строке записано два целых числа n, k (1≤k≤n≤5⋅10^5).
  Во второй строке записана строка длины nn, состоящая только
  из нулей и единиц. Ноль обозначает свободную клетку, а единица
  - занятую.
  Выведите единственное число - количество способов добраться
  до последней клетки. Поскольку это число может быть очень
  большим, выведите его по модулю 10^9 + 7.
 */

#include <iostream>
#include <vector>

using namespace std;

vector<long long> dp;
vector<long long> prefix;

long long get(int i) {
    return (i < 0) ? 0 : dp[i];
}

long long get_prefix(int i) {
    return (i < 0) ? 0 : prefix[i];
}

long long norm(long long a, long long mod) {
    return ((a % mod) + mod) % mod;
}

long long add(long long a, long long b, long long mod) {
    return norm(norm(a, mod) + norm(b, mod), mod);
}

long long sub(long long a, long long b, long long mod) {
   return norm(norm(a, mod) - norm(b, mod), mod);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int mod = 1000000000 + 7;
    int n, k;
    cin >> n >> k;
    string a;
    cin >> a;
    dp.assign(n + 1, 0);
    prefix.assign(n + 1, 0);
    dp[0] = 1;
    prefix[0] = 1;
    
    for (int i = 1; i <=n; ++i) {
        if (a[i-1] == '0') {
             dp[i] = sub(get_prefix(i - 1), get_prefix(i - k - 1), mod);
        }
        prefix[i] = add(prefix[i - 1], get(i), mod); 
    }
    cout << dp[n] << endl;
    return 0;
}
