/*
  Найдите, сколько существует строк заданной длины n,
  состоящих только из символов a, b и c,
  и не содержащих подстроки ab. 
  Во входном файле задано n (0≤n≤22).
  Выведите количество таких строк.
 */

#include <iostream>

using namespace std;

const int N = 25;
int array[N];

int main() {
    int n;
    cin >> n;
    array[0] = 1;
    array[1] = 3;
    
    for (int i = 2; i <=n; ++i) {
        array[i] = 3 * array[i - 1] - array[i - 2];
    }
    cout << array[n] << endl;
    return 0;
}

